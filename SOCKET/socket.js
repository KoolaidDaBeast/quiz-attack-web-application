var app = require('express')();
const cors = require('cors');
const fileupload = require('express-fileupload');

app.use(cors());
app.use(fileupload());

const server = require('http').Server(app);
const io = require('socket.io')(server);
var middleware = require('socketio-wildcard')();

const port = process.env.PORT || 3000;
io.use(middleware);

console.log("Server will be listening on port " + port);
server.listen(port);

function emit(io, namespace, room, data) {
    io.of('/' + namespace).emit(room, JSON.stringify(data));
}

app.get('/*', function (req, res) {
    return res.send('This socket test is working!');
});

io.on("connection", function (socket) {

    socket.on('*', function(packet){
        const event = packet.data[0];
        const eventData = packet.data[1]
        io.emit(event, eventData);
        console.log(event + "=" + eventData);
    });
});