const Accounts = require('./models/account');
const Posts = require('./models/post');
const Topics = require('./models/topic');
const Quiz = require('./models/quiz');

const crypto = require('crypto');
const mailer = require('nodemailer');
var email, emailPassword;

module.exports = {

    //Misc
    hash: function(secert, value) {
        var hash = crypto.createHmac('sha256', secert)
            .update(value)
            .digest('hex');

        if (hash.length > 256) hash = hash.substr(0, 255);

        return hash;
    },

    setEmailDetails: function(username, password) {
        email = username;
        emailPassword = password;
    },

    sendEmail: function(to, subject, text) {
        var transporter = mailer.createTransport({
            service: 'gmail',
            auth: {
                user: email,
                pass: emailPassword
            }
        });

        var mailOptions = {
            from: email,
            to: to,
            subject: subject,
            text: text
        };

        transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
                console.log(error);
                return false;
            } else {
                return true;
            }
        });
    },

    //Account Management
    emailExists: async function(emailAddress) {
        var promise = Accounts.findOne({ email: emailAddress.toLowerCase() });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    apiKeyExists: async function(key) {
        var promise = Accounts.findOne({ apiKey: key });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    displayNameExists: async function(name) {
        var promise = Accounts.findOne({ displayName: name });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    getUserDetails: async function(id) {
        var promise = Accounts.findOne({ _id: id }, (err, document) => {});

        const promiseValue = await promise;

        if (promiseValue == {}) { return {}; }

        var jsonData = { "displayName": promiseValue.displayName, "profileImage": promiseValue.profileImage, "misc": promiseValue.misc };

        return JSON.stringify(jsonData);
    },

    getUserId: async function(key) {
        var promise = Accounts.findOne({ apiKey: key }, (err, document) => {});

        const promiseValue = await promise;

        return promiseValue._id;
    },

    createAccount: async function(emailAddress, pswd, displayName, profileImage) {
        emailAddress = emailAddress.toLowerCase();
        var verfCode = this.hash(new Date().getTime().toString(), pswd);

        const newAccount = new Accounts({
            apiKey: this.hash(pswd, emailAddress),
            displayName,
            email: emailAddress,
            password: this.hash(emailAddress, pswd),
            profileImage,
            verified: verfCode,
            misc: JSON.stringify({})
        });

        this.sendEmail(emailAddress, "Verify Your Account", "Please verify your account at the following link: " + verfCode);

        var promise = newAccount.save(err => {});

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? true : false;
    },

    authenticateAccount: async function(emailAddress, password) {
        emailAddress = emailAddress.toLowerCase();

        var promise = Accounts.findOne({ email: emailAddress }, (err, document) => {});

        const promiseValue = await promise;

        if (promiseValue.email == emailAddress && promiseValue.password == this.hash(emailAddress, password)) {
            return promiseValue.apiKey;
        }

        return false;
    },

    verifyAccount: async function(param) {
        var promise = Accounts.findOne({ verified: code }, (err, document) => {
            document.verified = '';

            document.save((err) => {
                if (err) { console.log(err); }
            });
        });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    updateUserDetails: async function(key, displayName, img, misc) {
        var promise = Accounts.findOne({ apiKey: key }, (err, document) => {
            if (displayName != undefined) {
                document.displayName = displayName;
            }

            if (img != undefined) {
                document.profileImage = img;
            }

            if (misc != undefined) {
                document.misc = misc;
            }

            document.save((err) => {
                if (err) { console.log(err); }
            });
        });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    getTopics: async function() {
        var promise = Topics.find({});

        const promiseValue = await promise;

        return promiseValue == null ? {} : promiseValue;
    },

    getPosts: async function(topicId) {
        var promise = Posts.find({ topic_id: topicId }, (err, document) => {}).sort({ sent_at: -1 }).limit(20);

        const promiseValue = await promise;

        return promiseValue == null ? {} : promiseValue;
    },

    createPost: async function(key, topicId, content, image) {

        const newPost = new Posts({
            owner_id: await this.getUserId(key),
            topic_id: topicId,
            content: content,
            image: image,
            misc: JSON.stringify({ "likes": "", "comments": "" }),
            sent_at: new Date().getTime()
        });

        var promise = newPost.save(err => {});

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? true : false;
    },

    updatePost: async function(id, misc) {
        var promise = Posts.findOne({ _id: id }, (err, document) => {
            if (misc != undefined) {
                document.misc = misc;
            }

            document.save((err) => {
                if (err) { console.log(err); }
            });
        });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    createQuiz: async function(key, icon, title, topicId, metadata, status) {

        const newQuiz = new Quiz({
            owner_id: await this.getUserId(key),
            icon: icon,
            title: title,
            topic_id: topicId,
            metadata: metadata,
            status: status,
            created_at: new Date().getTime()
        });

        var promise = newQuiz.save(err => {});

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? true : false;
    },

    updateQuiz: async function(key, id, icon, title, topicId, metadata, status) {
        var promise = Quiz.findOne({ _id: id }, (err, document) => {

            document.icon = icon;
            document.title = title;
            document.topic_id = topicId;
            document.metadata = metadata;
            document.status = status;

            document.save((err) => {
                if (err) { console.log(err); }
            });
        });

        const promiseValue = await promise;

        return promiseValue == null || promiseValue == undefined ? false : true;
    },

    getQuizzes: async function(topicId) {
        var promise = Quiz.find({ topic_id: topicId, status: "true" }, (err, document) => {}).sort({ created_at: -1 }).limit(10).select('topic_id title icon owner_id created_at');

        const promiseValue = await promise;

        return promiseValue == null ? {} : promiseValue;
    },

    getCreatedQuizzes: async function(key) {
        const id = await this.getUserId(key);

        var promise = Quiz.find({ owner_id: id }, (err, document) => {}).sort({ created_at: -1 }).limit(10).select('topic_id title icon owner_id created_at');

        const promiseValue = await promise;

        return promiseValue == null ? {} : promiseValue;
    },

    getQuiz: async function(key, quizId) {
        const id = await this.getUserId(key);

        var promise = Quiz.find({ owner_id: id, _id: quizId }, (err, document) => {}).sort({ created_at: -1 });

        const promiseValue = await promise;

        return promiseValue == null ? {} : promiseValue;
    },

}