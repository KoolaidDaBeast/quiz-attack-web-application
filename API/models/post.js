const mongoose = require('mongoose');

module.exports = mongoose.model('Posts', new mongoose.Schema({
    owner_id: String,
    topic_id: String,
    content: String,
    image: String,
    misc: String,
    sent_at: String
}));