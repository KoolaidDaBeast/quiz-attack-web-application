const mongoose = require('mongoose');

module.exports = mongoose.model('Topics', new mongoose.Schema({
    topic_id: String,
    name: String
}));